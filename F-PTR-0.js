function pascal(n)
{
	if (n < 0 || n % 1 != 0)
		console.log ([]);
	else
	{
		var row = [1];
		for (var i = 0; i < n; i++)
		{
			var rowTemp = [1];
			for (var j = 0; j < row.length - 1; j++) 
			{
				rowTemp.push(row[j] + row[j + 1]);
			}
			rowTemp.push(1);
			row = rowTemp;
		}
		console.log(row);
	}
}