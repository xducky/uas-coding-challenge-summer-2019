#include <iostream>
#include <cmath>

using namespace std;

double findSCP (double x1, double y1, double x2, double y2, double xc, double yc);
double findDistance (double x, double y, double xc, double yc);

int main()
{
    double x1, y1, x2, y2, xc, yc, r, SCP;
    int fin;

    cout << "Enter in format: x1 y1 x2 y2 xCircle yCircle radius" << endl;

    cin >> x1 >> y1 >> x2 >> y2 >> xc >> yc >> r;

    if (x1 == x2 || y1 == y2)
    {
    	if (x1 == x2)
    		SCP = abs (x1 - xc);
    	else
    		SCP = abs (y1 - yc);
    }
    else
    	SCP = findSCP (x1, y1, x2, y2, xc, yc); //distance of Single Closest Point on the line to the center of circle

   	if (SCP <= r)
    	if (SCP == r)
    		cout << "One Intersection";
    	else 
    		cout << "Two Intersection";
    else 
    	cout << "No Intersection";

    cin >> fin;
}

double findSCP (double x1, double y1, double x2, double y2, double xc, double yc)
{
	double m = (y1 - y2) / (x1 - x2);
	double mPerpendicular = 0 - ((x1 - x2) / (y1 - y2));
	double b = y1 - (m * x1);
	double b2 = yc - (mPerpendicular * xc);
	double SCPx = (b2 - b) / (m - mPerpendicular);
	double SCPy = (m * SCPx) + b;
	return findDistance (SCPx, SCPy, xc, yc);
}

double findDistance (double x, double y, double xc, double yc)
{
	return sqrt ( pow((x - xc), 2) + pow ((y - yc), 2));
}


